## § چگونگی اجرای برنامه در کلاستر

برای اجرای برنامه مورد نظر، ابتدا باید آن را در صف قرار دهید. نرم‌افزار SLURM برای اینکار در نظر گرفته شده است. ابتدا اسکریپت مورد نظرتان را می‌سازید که نمونه این فایل‌ها (job-script.sh) را برای نرم افزارهای مختلف می‌توانید [نمونه‌ها](Sample.md) ببینید.

## § در صف قرار دادن برنامه

در فایل job-script.sh (کادر سبز رنگ)، دستورات مرتبط با در صف قرار دادن و اجرای برنامه مورد نظرتان وجود دارد. شما باید فایل اسکریپت را در کنار فایل ورودی برنامه‌ی خودتان (کادر قرمز رنگ) قرار دهید. سپس با دستور زیر برنامه خود را در صف قرار می‌دهید:

<div style="text-align:center"><img src ="1.jpg" /></div>

پس از اجرای این دستور عددی را در ترمینال مشاهده خواهید کرد که به آن JOBID می گویند که برای پیگیری وضعیت برنامه و یا کنسل کردن آن می توانید از این عدد به روشی که در ادامه توضیح داده خواهد شد استفاده کنید.

<div style="text-align:center"><img src ="2.jpg" /></div>

پس از اتمام محاسبات، فایل های خروجی در کنار فایل ورودی شما تولید خواهند شد (در حین انجام محاسبات فایل هایی که بر روی هارددیسک کپی می شوند، برای شما قابل مشاهده خواهند بود).

**توجه: به حروف بزرگ و کوچک دقت کنید، چرا که محیط شل به این حروف حساس است.**

## § مشاهده ی وضعیت برنامه

برای فهمیدن وضعیت برنامه، از دستور squeue استفاده کنید. لیستی از برنامه های درحال اجرا و یا در نوبت اجرا را مشاهده خواهید کرد.

<div style="text-align:center"><img src ="3.jpg" /></div>

وضعیت R بیانگر در حال اجرا، وضعیت C بیانگر کنسل یا اتمام برنامه، وضعیت Q بیانگر این است که برنامه در صف قرار دارد.

## § کنسل کردن برنامه

به منظور قطع برنامه، از دستور زیر استفاده می کنید:

```bash
scancel <JOBID>
```

استفاده می‌کنیم که <JOBID>، شماره برنامه شما است که در دستور squeue مشخص می‌شود.

