## § اشخاص

شرکت ان‌ارتی‌سی (NRTC) لازم می‌داند مراتب قدردانی خود را از دانشگاه صنعتی شاهرود و به طور مشخص آقای [دکتر حمید حسن پور](http://shahroodut.ac.ir/fa/as/?id=S333) اعلام نمایید.

این شرکت همچنین لازم می‌داند از آقایان مهندس رضا رمضانیان، خلیل کلایی دارابی و سید مسلم حسینی برای کمک در امر تنظیم کارگزار دامنه و همچنین در اختیار گزاردن آی‌پی معتبر و بررسی شبکه همکاری نمودند، تشکر نماید. بدون کمک این عزیزان انجام این پروژه ناممکن به نظر می‌رسید.

<div style="direction: ltr"> Special thanks to Mr. <a href="https://www.researchgate.net/profile/Ivan_Girotto">Ivan Girotto</a> for his help, on compiling Quantum-Espresso using Intel® Compilers & Libs, during his visit for <a href="http://indico.ictp.it/event/7952/">5th Workshop on Advanced Techniques for Scientific Programming and Management of Open Source Software Packages</a> </div>

همچنین این حقیر لازم می‌داند از آقای دکتر مهدی فقیه نصیری مدیر محترم شرکت ان‌ارتی‌سی (NRTC) قدردانی نماید.

[امیرحسین فیروزیان](http://me.firouzian.ir)، مدیر تحقیقات و گسترش پردازش سریع شرکت ان‌آر‌تی‌سی (NRTC)، زمستان ۱۳۹۵

## § شرکت‌ها و بنیادها و مرکزها
<div style="text-align:center">
<img src="The_logo_of_Shahrood_University_of_Technology.jpg" alt="Drawing" style="width: 128px;display: inline;"/>

<img src="NRTC.png" alt="Drawing" style="display: inline;"/>

<img src="openlogo.svg" alt="Drawing" style="display: inline;"/>
</div>
